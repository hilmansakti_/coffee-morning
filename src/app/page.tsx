"use client";

import { useState } from "react";
import Head from "next/head";
import "@/app/globals.css";
import questions from "@/data/questions.json";
import pertanyaan from "@/data/pertanyaan.json";

const Home = () => {
  const [selectedOptions, setSelectedOptions] = useState([
    {
      answerByUser: "",
      answerType: "",
    },
  ]);
  const [nameValue, setNameValue] = useState("");
  const [name, setName] = useState("");
  const [amiable, setAmiable] = useState(0);
  const [driver, setDriver] = useState(0);
  const [analytical, setAnalytical] = useState(0);
  const [expressive, setExpressive] = useState(0);
  const [score, setScore] = useState(0);
  const [showScore, setShowScore] = useState(false);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [errorSetName, setErrorSetName] = useState(false);
  const [language, setLanguage] = useState("en");
  const [questionsData, setQuestionsData] = useState(questions);

  const handleAnswerOption = (answer: any, type: any) => {
    setSelectedOptions([
      (selectedOptions[currentQuestion] = {
        answerByUser: answer,
        answerType: type,
      }),
    ]);
    setSelectedOptions([...selectedOptions]);
  };

  const handleSubmitButton = () => {
    let amiableScore = 0;
    let expressiveScore = 0;
    let analyticalScore = 0;
    let driverScore = 0;
    for (let i = 0; i < selectedOptions.length; i++) {
      switch (selectedOptions[i]?.answerType) {
        case "amiable":
          amiableScore += 1;
          break;
        case "analytical":
          analyticalScore += 1;
          break;
        case "driver":
          driverScore += 1;
          break;
        case "expressive":
          expressiveScore += 1;
          break;
        default:
          console.log("wrong answer");
      }
    }
    setAmiable(amiableScore);
    setAnalytical(analyticalScore);
    setDriver(driverScore);
    setExpressive(expressiveScore);

    setShowScore(true);
  };

  const handleStart = () => {
    if (!nameValue) {
      setErrorSetName(true);
    }
    setName(nameValue);
  };

  const handleInputChange = (event: any) => {
    setNameValue(event.target.value);
  };

  const handlePrevious = () => {
    const prevQues = currentQuestion - 1;
    prevQues >= 0 && setCurrentQuestion(prevQues);
  };

  const handleNext = () => {
    const nextQues = currentQuestion + 1;
    nextQues < questionsData.length && setCurrentQuestion(nextQues);
  };

  const handleLanguage = () => {
    if (language === "en") {
      setQuestionsData(pertanyaan);
      setLanguage("id");
    } else {
      setQuestionsData(questions);
      setLanguage("en");
    }
  };

  return (
    <>
      <Head>
        <title>Quiz App</title>
      </Head>
      <div className="flex flex-col w-screen px-5 h-screen bg-[#1A1A1A] justify-center items-center">
        {showScore ? (
          <div style={{ color: "white" }}>
            <h1 className="text-3xl font-semibold text-center text-white mb-5">
              {language === "en"
                ? "Congratulations " + name + "! You have finished this test."
                : "Selamat " +
                  name +
                  "! Kamu telah menyelesaikan tes ini."}{" "}
              🥳🥳🥳
            </h1>
            <p className="mb-5">
              {language === "en"
                ? "here's your result :"
                : "ini hasil dari tes kamu :"}
            </p>

            <p>Amiable 😊 : {amiable}</p>
            <p>Analytical 📊: {analytical} </p>
            <p>Direct Driver 🎯 : {driver}</p>
            <p>Expressive 🤸‍♂️: {expressive}</p>
          </div>
        ) : !name ? (
          <>
            <div className="fixed top-10 w-full inline-flex items-end justify-end pr-20">
              <button
                className="inline-flex items-center justify-center px-5 py-3 text-base font-medium text-center text-white bg-gray-700 rounded-lg hover:bg-gray-800 focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-900 mt-5"
                onClick={handleLanguage}
              >
                {language === "en" ? "EN" : "ID"}
              </button>
            </div>
            <h1 className="mb-4 text-center text-2xl font-extrabold leading-none tracking-tight text-gray-900 md:text-4xl lg:text-5xl dark:text-white">
              {language === "en"
                ? " Hi Indies, Let's Recognizing Your Communication Style!"
                : "Hai Indies, Ayo Kenali Gaya Komunikasimu!"}
            </h1>
            <p className="mb-6 text-lg font-normal text-center text-gray-500 lg:text-xl sm:px-26 xl:px-48 dark:text-gray-400">
              {language === "en"
                ? "This test is designed to identify your communication style. There are no right or wrong answers. Just choose the answer that best describes you. Before you start, please fill your name below. Good luck! 🚀🚀🚀"
                : "Tes ini dirancang untuk mengidentifikasi gaya komunikasi kamu. Tidak ada jawaban yang benar atau salah. Cukup pilih jawaban yang paling sesuai dengan kamu. Sebelum kamu memulai, silakan isi nama kamu di bawah ini. Semoga berhasil! 🚀🚀🚀"}
            </p>

            <form className="w-full max-w-sm">
              <div className="flex items-center border-b border-white text-white py-2">
                <input
                  className="appearance-none bg-transparent border-none w-full text-white mr-3 py-1 px-2 leading-tight focus:outline-none"
                  type="text"
                  placeholder="Chandra A Nugo"
                  aria-label="Full name"
                  value={nameValue}
                  onChange={handleInputChange}
                />
              </div>
            </form>

            {errorSetName && (
              <p className="text-red-400 mt-5">
                {language === "en"
                  ? "Please fill your name ☹️☹️☹️"
                  : "Isi nama kamu dulu ☹️☹️☹️"}
              </p>
            )}

            <button
              className="inline-flex items-center justify-center px-5 py-3 text-base font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-900 mt-5"
              onClick={handleStart}
            >
              {language === "en" ? "Let's Start" : "Ayo Mulai"}
              <svg
                className="w-3.5 h-3.5 ml-2"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M1 5h12m0 0L9 1m4 4L9 9"
                />
              </svg>
            </button>
          </>
        ) : (
          <>
            <div className="flex flex-col items-start w-full">
              <h4 className="mt-10 text-xl text-white/60">Good luck {name}</h4>
              <h4 className="mt-10 text-xl text-white/60">
                {language === "en" ? "Question " : "Pertanyaan "}
                {currentQuestion + 1} {language === "en" ? "of " : "dari "}{" "}
                {questionsData.length}
              </h4>
              <div className="mt-4 text-2xl text-white mb-4">
                {questionsData[currentQuestion].questionText}
              </div>
            </div>
            <div className="flex flex-col w-full">
              {questionsData[currentQuestion].answerOptions.map(
                (answer, index) => (
                  <div
                    key={index}
                    className="flex items-center w-full py-4 pl-5 m-2 ml-0 space-x-2 border-2 cursor-pointer border-white/10 rounded-xl bg-white/5"
                    onClick={(e) =>
                      handleAnswerOption(answer.answer, answer.type)
                    }
                  >
                    <input
                      type="radio"
                      name={answer.answer}
                      value={answer.answer}
                      checked={
                        answer.answer ===
                        selectedOptions[currentQuestion]?.answerByUser
                      }
                      onChange={(e) =>
                        handleAnswerOption(answer.answer, answer.type)
                      }
                      className="w-6 h-6 bg-black"
                    />
                    <p className="ml-6 text-white">{answer.answer}</p>
                  </div>
                )
              )}
            </div>
            <div className="flex justify-between w-full mt-4 text-white">
              {currentQuestion > 0 && (
                <button
                  onClick={handlePrevious}
                  className="inline-flex items-center justify-center px-5 py-3 text-base font-medium text-center text-white bg-gray-700 rounded-lg hover:bg-gray-800 focus:ring-4 focus:ring-gray-300 dark:focus:ring-gray-900 mt-5"
                >
                  {language === "en" ? "Previous" : "Sebelumnya"}
                </button>
              )}
              <button
                onClick={
                  currentQuestion + 1 === questionsData.length
                    ? handleSubmitButton
                    : handleNext
                }
                className="inline-flex items-center justify-center px-5 py-3 text-base font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-900 mt-5"
              >
                {currentQuestion + 1 === questionsData.length
                  ? language === "en"
                    ? "Finish"
                    : "Selesai"
                  : language === "en"
                  ? "Next"
                  : "Selanjutnya"}
                {currentQuestion + 1 !== questionsData.length && (
                  <svg
                    className="w-3.5 h-3.5 ml-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                  >
                    <path
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M1 5h12m0 0L9 1m4 4L9 9"
                    />
                  </svg>
                )}
              </button>
            </div>
          </>
        )}
      </div>
      <footer className="w-full bg-white mt-10 dark:bg-gray-800">
        <div className="max-w-screen-xl mx-auto p-4 md:flex md:items-center md:justify-center">
          <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
            {language === "en"
              ? "This platform is developed collaboratively by "
              : "Platform ini dikembangkan secara kolaboratif oleh "}
            <b>Indies.</b>
          </span>
        </div>
      </footer>
    </>
  );
};

export default Home;
